// --------- NOTES: ---------

// BUILDING OBJECTS
// Class in javascript is not object but it's used to DEFINE object.

// Function constructor -- Constructing an object using a function. Just a normal function that is used to construct objects.
// function Person (firstname, lastname) {
//   console.log(this) // Person {}
//   this.firstname = firstname
//   this.lastname = lastname
//   console.log('invoked')
// }

// var john = new Person('john', 'doe')
// console.log(john)

// var jane = new Person('jane', 'doe')
// console.log(jane)
// "new" keyword will "return" a new empty object and the "this" keyword will then be pointed to the new empty object.
// As long as you don't return anything from the function, "new" function will always return the object created.
// "new" keyword CREATES the object, and function constructors add methods and properties to the object.

// function Person (firstname, lastname) {
//   console.log(this) // Person {}
//   this.firstname = firstname
//   this.lastname = lastname
//   console.log('invoked')

//   this.greet = function () {
//     console.log('hello')
//   }
// }

// Person.prototype.getFullName = function () {
//   return this.firstname + ' ' + this.lastname
// }

// var john = new Person('john', 'doe')
// console.log(john.getFullName())
// // console.log(john.getOfficialName()) // wont work here

// var jane = new Person('jane', 'doe')
// // console.log(jane.__proto__)
// console.log(jane)

// Person.prototype.getOfficialName = function () { // this is assigning, so it wont be hoisted during init phase
//   return this.lastname + ', ' + this.firstname
// }

// console.log(john.getOfficialName())
// if a method is added to the object during function construction phase, when you console.log the object, you can see the method being listed out as a part of the object.
// but if the method is added to the prototype of the object, it will now show up as a part of the object, but the function is still fully functional.

// var a = new Number(2)
// var john = new String('john')
// console.log(a)    // Number {2}
// console.log(john) // String {'john'}
// // function constructors will make it looks like you're creating primitives but it's actually creating an object that CONTAINS primitives.
// // function constructors always returns an object.

// String.prototype.isLengthGreaterThan = function (limit) {
//   return this.length > limit
// }
// console.log(john.isLengthGreaterThan(3)) // true

// // console.log(3.toFixed(3)) // syntax error
// Number.prototype.isPositive = function () {
//   return this > 0
// }
// var three = new Number(3)
// console.log(three.isPositive()) // true
// by using the built-in function constructors to create primitives, you're not exactly creating a primitives but an object containing the primitive values.


// ARRAYS AND FOR..IN
// for..in is used to loop thru an object
// var obj = {a: 1, b: 2, c: 3}
// for (var prop in obj) {
//   console.log(prop + ': ' + obj[prop])
//   // a: 1
//   // b: 2
//   // c: 3
// }

// and because array is an object too, so you can do for..in on an array
// var arr = ['john', 'jane', 'jim']
// for (var index in arr) {
//   console.log(index + ': ' + arr[index])
//   // 0: john
//   // 1: jane
//   // 2: jim
// }
// When arrays are created, each element in the array is assigned to an index number as the key/property to this array object that contains the value of the element you passed in.
// That's why you can do arr[indexnumber] to get the value of the element in the array.
// It's essentially doing object[property] to get the value of the element. Because index number is the property of the array object.

// var arr = ['john', 'jane', 'jim']

// Array.prototype.customProp = 'hi there'

// for (var index in arr) {
//   console.log(index + ': ' + arr[index])
//   // 0: john
//   // 1: jane
//   // 2: jim
//   // customProp: hi there
// }

// console.log(arr) // ['john', 'jane', 'jim']
// It's best to just use the normal for loop to loop thru arrays to avoid unexpected results.


// Object.create() AND PURE PROTOTYPICAL INHERITANCE
// **only functions will give you new execution context

// var person = {
//   fn: 'default',
//   ln: 'default',
//   greet: function () {
//     console.log('hello ' + this.fn)
//   }
// }

// var john = Object.create(person)
// john.fn = 'john'
// john.ln = 'doe'
// console.log(john) // {} in browser, but props are still accessible.


// POLYFILL: codes that adds a feature which the engine may lack (older engines)

// this is polyfilling for older browsers that don't have Object.create method
// if (!Object.create) {
//   Object.create = function (obj) {
//     if (arguments.length > 1) {
//       throw new Error('Object.create implementation' + ' only accepts the first parameter.')
//     }

//     function F() {}
//     F.prototype = obj
//     return new F()
//   }
// }

// var person = {
//   fn: 'default',
//   ln: 'default',
//   greet: function () {
//     console.log('hello ' + this.fn)
//   }
// }

// var john = Object.create(person)
// john.fn = 'john'
// john.ln = 'doe'
// console.log(john) // {} in browser, but props are still accessible.


// ES6 AND CLASSES
// SYNTACTIC SUGAR: A different way to write/type something that doesn't change how it works under the hood.
// class Person {
//   constructor (data) {
//     this.firstname = data.firstname
//     this.lastname = data.lastname
//   }
// }

// class informalPerson extends Person {
//   constructor (firstname, lastname) {
//     super(firstname, lastname)
//   }

//   greet () {
//     console.log('whaddup ' + this.firstname)
//   }
// }

// var john = new informalPerson({firstname: 'john', lastname: 'smith'})
// console.log(john)
// john.greet()
