// --------- NOTES: ---------

// ===== TYPES
// js is dynamic typing
// the type of data a variable is holding will be "defined" during execution phase
// eg: in C++ you need to define datatype for each variable during creation phase (static typing)

// Static Typing
// bool isNew = 'hello'; // gives error because bool type expects true / false, not a string

// Primitive Type
// type of data that represents ONE single value (not object, because obj is COLLECTION of key/value pairs)
// 1. undefined
// 2. null
// 3. boolean
// 4. number (the only numeric type, and it's floating point. ie: there's always decimals)
// 5. string
// 6. symbol (ES6 only)


// ===== OPERATORS
// Special functions that's syntatically different / written differently
// just functions that take TWO params and return ONE result
// . (dot) is also operator!

// operator position and their names when in such positions
// 3 + 4 --> infix notation   (between 2 params)
// +3 4  --> prefix notation  (in front params)
// 3 4+  --> postfix notation (at the end of params)

// var a = 2, b = 3, c = 4;
// a = b = c;

// console.log(a);
// console.log(b);
// console.log(c);
// everything is 4


// ===== COERCION
// converting a value from one type to another
// + operator will try to stringify values when one of the param is a string
// < or > will coerce boolean to number, because Number(true) returns 1; Number(false) returns 0
// value in between () of an IF statement, will be coerced into a boolean

// var a = NaN + '1';
// console.log(a);     // 'NaN1' as string
// console.log(3 < 2 < 1) // true, because false < 1 becomes 0 < 1, so it's true


// ===== DEFAULT VALUES
// function greet(name = 'joe') { // default value in passed parameters are not supported in older browser
//   // name = name || 'joe';
//   console.log('hello ' + name);
// }

// greet('el');

console.log(libName)
