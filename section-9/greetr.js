// --------- NOTES: ---------

// CREATE YOUR OWN FRAMEWORK / LIBRARY

// Requirements

// GREETR
// When given a first name, last name, and optional language, it generates formal and informal greetings.
// Support English and Spanish languages.
// Reusable library/framework
// Easy to type "G$()" structure
// Support jQuery


;(function (global, $) {
  'use strict';

  var Greetr = function (firstname, lastname, language) {
    return new Greetr.init(firstname, lastname, language)
  }

  // vars created in IIFE are like "private variables", can be accessed by everything within the IIFE but not from the outside.
  // Greetr objects created can still access these because they are "closed-in" by closure
  var
  supportedLan = ['en', 'es'],

  greetings = {
    en: 'Hello',
    es: 'Hola'
  },

  formalGreetings = {
    en: 'Greetings',
    es: 'Saludos'
  },

  logMsgs = {
    en: 'logged in',
    es: 'Inicio sesion'
  }

  Greetr.prototype = { // this empty object should be where you store methods, so the object returned from init() should be pointing to this.
    fullname: function () {
      return this.firstname + ' ' + this.lastname
    },

    validate: function () {
      if (supportedLan.indexOf(this.language) === -1) {
        throw 'Invalid language'
      }
    },

    greeting: function () {
      return greetings[this.language] + ' ' + this.firstname + '!'
    },

    formalGreeting: function () {
      return formalGreetings[this.language] + ', ' + this.fullname()
    },

    greet: function (formal) {
      var msg

      if (formal) {
        msg = this.formalGreeting()
      } else {
        msg = this.greeting()
      }

      if (console) { // IE doesn't have console obj unless console is opened
        console.log(msg)
      }

      return this // just to make this method chainable
    },

    log: function () {
      if (console) {
        console.log(logMsgs[this.language] + ': ' + this.fullname())
      }

      return this
    },

    setLang: function (lan) {
      // supportedLan.push(lan)
      this.language = lan
      this.validate() // only letting you change to the supported languages
      return this
    },

    showGreet: function (selector, formal) {
      if (!$) {
        throw 'jQuery not found'
      }

      if (!selector) {
        throw 'missing selector'
      }

      var msg
      if (formal) {
        msg = this.formalGreeting()
      } else {
        msg = this.greeting()
      }

      $(selector).html(msg)
      return this
    }
  }

  Greetr.init = function (firstname, lastname, language) {
    var self = this // "this" is still pointing to greetr.init() anthony is just making sure that the ref is correct.
    self.firstname = firstname || ''
    self.lastname  = lastname  || ''
    self.language  = language  || 'en'

    self.validate()
  }

  Greetr.init.prototype = Greetr.prototype // points the object returned from init() to the prototype, so when you add methods to .prototype, the newly created object will have access to the Greetr base object

  global.g$ = global.Greetr = Greetr


})(window, jQuery)
