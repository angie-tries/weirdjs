// --------- NOTES: ---------

// OBJECT ORIENTED JAVASCRIPT AND PROTOTYPAL INHERITANCE
// inheritance = one obj gets access to the properties and methods of another object
// var person = {
//   fn: 'default',
//   ln: 'default',
//   mn: 'ey',
//   getFullName: function () {
//     return this.fn + ' ' + this.ln
//   }
// }

// var john = {
//   fn: 'John',
//   ln: 'Smith'
// }

// DEMO PURPOSE ONLY do not ever do this!
// the below will dramatically slow down your application
// john.__proto__ = person
// console.log(john.getFullName())
// console.log(john.mn)

// var jane = {
//   fn: 'Jane'
// }

// jane.__proto__ = person
// console.log(jane.getFullName())

// __proto__ will get you ONE LEVEL acesss to the model of the constructor
// and that is why all functions have access to .call, .bind, and .apply etc methods
// var a = {}
// var b = function () {}
// var c = []

// // if you run this in the browser console
// a.__proto__            // Object {}
// b.__proto__            // f () { [native code] }
// c.__proto__.__proto__  // Object {}   <----- BECAUSE ARRAY IS AN OBJECT


// REFLECTION AND EXTEND
// reflection = An object can look at itself, listing and changing its properties and methods.
// var person = {
//   fn: 'default',
//   ln: 'default',
//   getFullName: function () {
//     return this.fn + ' ' + this.ln
//   }
// }

// var john = {
//   fn: 'John',
//   ln: 'Smith'
// }

// john.__proto__ = person

// // looping thru OBJECT
// // for (var prop in john) {
// //   if (john.hasOwnProperty(prop)) { // this method will only access property of the object being called, not the constructor
// //     console.log('John\'s prop: ' + prop + ' is ' + john[prop])
// //   }
// //   console.log(prop + ': ' + john[prop])
// // }

// var jane = {
//   fn: 'Jane',
//   ln: 'Smith',
//   getOfficialName: function () {
//     return this.ln + ', ' + this.fn
//   }
// }

// var jim = {
//   address: 'kuala lumpur'
// }

// _.extend(john, jane, jim)
// console.log(john) // changes made in underscore.js to get intended result.
// // it worked, but not sure why person's method isn't displayed out when john is console logged.
// // the method is fully functional tho.
