// --------- NOTES: ---------

// ODDS AND ENDS

// typeof and instanceof
// var a = 4
// console.log(typeof a) // number

// var b = 'hi'
// console.log(typeof b) // string

// var c = {}
// console.log(typeof c) // object

// var d = []
// console.log(typeof d) // object, because array is an object
// console.log(Object.prototype.toString.call(d)) // [object Array]
// // https://medium.com/better-programming/what-is-object-object-in-javascript-object-prototype-tostring-1db888c695a4

// function Person (name) {
//   this.name = name
// }

// var e = new Person('albert')
// console.log(typeof e) // object
// console.log(e instanceof Person) // true // because instanceof will go down the prototype chain and check if e was ever a prototype of Person, if yes, this returns true

// console.log(typeof undefined) // undefined
// console.log(typeof null) // object <-- this is a javascript bug

// var z = function () {}
// console.log(typeof z) // function


// STRICT MODE
// Telling javascript engine to parse codes stricter
// Every javascript engines implements 'use strict' the same way
// If you minify your file, everything AFTER 'use strict' will be parsed strictly (might cause trouble)
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode

// function logNewPerson() {
//   'use strict'

//   var person2
//   persom2 = {}
//   console.log(persom2)
// }

// // 'use strict'
// var person
// persom = {}
// console.log(persom) // without use strict, persom is {}
// logNewPerson()
