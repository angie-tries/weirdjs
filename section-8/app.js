// --------- NOTES: ---------

// Examining "famous" frameworks
// var q = $('ul.people li')
// console.log(q)
// Basically, jQuery is just a giant library that "helps" with typing javascript.
// According to the source code, $ returns a new jQ object that you can later do something with it.
// and source code then extends the new jQ object to the prototype so the methods sit on base prototype and not the object itself to conserve memory.
