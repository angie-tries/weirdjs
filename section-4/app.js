// --------- NOTES: ---------

// all JSON syntax is valid js literal (but not vice versa, duh, how do you stringify functions!)
// functions are special type of object

// First Class Functions / First Class Objects
// everything you can do with other types, you can do it with functions
// ie:
// store in var
// passed as arguments
// return from functions

// you can attach properties to functions (because it's an object)
// function greet() {
//   console.log('hi')
// }
// greet.language = 'english'
// console.log(greet.language) // 'english'

// function expression = code that returns a value
// function statement = code that doesn't return a value (ie: IF statements)

// this is a function statement (setting up of a function during creation phase)
// function greet() {
//   console.log('hi')
// }

// if you assign anon function to var, you cannot call it before the line.
// because during creation phase, only variable name is hoisted, with default value undefined.
// and you cannot invoke undefined.
// anonGreet() // wont work
// var anonGreet = function () {
//   console.log('hi')
// }

// Primitive value variables are passed by value
// Objects are passed by reference (all objects including functions)

// var a = {x: 1}
// var b = a
// console.log(a, b) // {x: 1}, {x: 1}

// a = {y: 2} // assigned new obj, = operator setup new memory space
// console.log(a, b) // {y: 2}, {x: 1}

// var person = {
//   name: 'John',
//   greet: function () {
//     this.name = 'Johnny'
//     var self = this // without this reference

//     var setname = function (newname) { // this function is tied to global obj 🤷‍♂️
//       self.name = newname
//     }
//     setname('Billy')
//     console.log(this)
//   }
// }

// person.greet()


// ARGUMENTS
// Array-like = looks like array, acts like array, but it's not array
// [] to access, .length, for loop

// function greet(name, language, ...others) {
//   console.log(others) // [true, 1]  // wraps everything after declared arg into ONE arr
//   console.log(arguments)
// }

// greet('hello', 'en', true, 1)


// AUTOMATIC SEMICOLON
// js parser will insert semicolons for you automatically whenever its expecting


// IIFEs
const x = function hello(name) {
  console.log('hi ' + name)
}

'hello' // this is valid js.
true    // these are considered expression, it will just return whatever that's being fed

// function(name) {
//   return 'hi ' + name
// }

// the above function is invalid, because when a function is declared at the BEGINNING of a line,
// it's expecting the function to have a name, unless you wrap it in paranthesis.
// turning the function into a function expression (IIFE)
// so below are valid
// function hellos(name) {
//   console.log('hello ' + name)
// }

// (function(name) {
//   var greeting = 'hello'
//   console.log(greeting + ' ' + name)
// }('john'))

// ;(function(name) {     // please make it a habit to put a semicolon before an IIFE to avoid errors from auto semicolon insertion
//   var greeting = 'hello'
//   console.log(greeting + ' ' + name)
// })('john2')

// but why can't i do this?
// (return function(name) {
//   var greeting = 'hello'
//   console.log(greeting + ' ' + name)
// })('john2')


// CLOSURES
// function greet(msg) {
//   return function(name) {
//     console.log(msg + ' ' + name)
//   }
// }

// const sayHi = greet('hi')
// sayHi('sally') // hi sally

// when greet is created and invoked, 'hi' was created in the execution context of greet.
// once greet is done running, greet will be popped off the execution stack.
// altho greet's execution context was removed, 'hi' was still left in the stack before js's garbage collector does its job.
// so when sayHi is invoked, its execution context will be created. it will first look for 'hi' in its execution context.
// and when it couldn't locate 'hi' in it, it will look out one of execution context, which is where 'hi' is located.

// CLOSURES - MORE EXAMPLES
// function buildFn() {
//   const arr = []

//   for (var i = 0; i < 3; i++) {
//     arr.push(
//       function() {
//         console.log(i)
//       }
//     )
//   }

//   return arr
// }

// const fns = buildFn()
// fns[0]() // 3
// fns[1]() // 3
// fns[2]() // 3

// The above are 3 because variable 'i' is 3 when the for loop finished running.
// and 'i' was updated before the annon functions were called.
// So, in fns[x] functions, their 'i's are referring to the same variable created in for loop if statement.
// And because fns[x] functions were CREATED IN THE SAME PLACE LEXICALLY, their 'i's will always be referring to the same variable.

// However, the below example yields a very different output.

// function buildFn2() {
//   const arr = []

//   for (let i = 0; i < 3; i++) {
//     arr.push(
//       function() {
//         console.log(i)
//       }
//     )
//   }

//   return arr
// }

// const fns2 = buildFn2()
// fns2[0]() // 0
// fns2[1]() // 1
// fns2[2]() // 2

// This is because you are using 'let' to declare variable for 'i'.
// 'let' creates a new "invisible" block scope.
// Meaning: The FIRST function (fns2[0]) during invocation, has reference to 'i' that was sitting in the memory somewhere, and that 'i' was assigned the value '0' before it was incremented by 'i++'
// vice versa for the rest of the function.
// MORE INFO ABOUT LET & CONST BLOCK SCOPES: https://hacks.mozilla.org/2015/07/es6-in-depth-let-and-const/

// variables that are outside of a function but still available are called "free variables".

// Get 0,1,2 with ES5
// function buildFn3() {
//   const arr = []

//   for (var i = 0; i < 3; i++) {
//     arr.push(
//       (function(j) {
//         return function () {
//           console.log(j)
//         }
//       })(i)
//     )
//   }

//   return arr
// }

// const fns3 = buildFn3()
// fns3[0]() // 0
// fns3[1]() // 1
// fns3[2]() // 2

// function makeFn (language) {
//   return function greet(fn, ln) {
//     if (language === 'en') {
//       console.log('hello ' + fn + ' ' + ln)
//     } else if (language === 'es') {
//       console.log('hola ' + fn + ' ' + ln)
//     }
//   }
// }

// var hiEn = makeFn('en');
// var hiEs = makeFn('es');

// hiEn('john', 'doe')
// hiEs('john', 'doe')

// language in hiEn() will not be referring to the new hiEs because those are two different execution contexts
// everytime you call a function, it will create a new execution context


// CLOSURES AND CALLBACKS
// A callback function is a function that you pass when invoking a function.
// The function you passed will be executed once the first invoked function is done running.

// function sayHiLater (msg) {
//   var greeting = 'hi'

//   setTimeout(function() {
//     console.log(msg)
//   }, 3000)
// }
// sayHiLater('hello')

// jQuery example:
// $('button').click(function(){})
// This click function takes a callback function.

// function tellMeWhenDone(done) {
//   var a = 1000
//   var b = 2000

//   done(a, b)
// }

// tellMeWhenDone(function() {
//   console.log('done')
// })

// tellMeWhenDone(function(a, b) {
//   window.alert('done')
// })


// CALL(), APPLY(), BIND()
// var person = {
//   fn: 'John',
//   ln: 'Doe',
//   getFullName: function() {
//     var fullname = this.fn + ' ' + this.ln
//     return fullname
//   }
// }

// var logName = function (lang1, lang2) {
//   console.log('Log: ' + this.getFullName())
//   // console.log('Log: ' + window.person.getFullName()) // this will work because person is tied to window object when it's declared with var and not let / const
//   console.log('params: ' + lang1 + ' ' + lang2)
//   console.log('------')
// } // .bind(person) // creates a function on the fly and binds this function to person

// var logPersonName = logName.bind(person)
// logPersonName('es') // bind creates a copy of the function after tying it to the object but doesn't run it

// // logName() // this will throw error unless calling it with call() and pass in the object
// logName.call(person, 'cn', 'jp')
// logName.apply(person, ['th', 'kr']) // apply takes an array as second argument

// ;(function(a, b) {
//   console.log('Log: ' + this.getFullName())
//   console.log(a, b)
//   console.log('>>>>>')
// }).apply(person, ['x', 'y'])

// FUNCTION BORROWING
// var person2 = {
//   fn: 'Jane',
//   ln: 'Doe'
// }

// console.log(person.getFullName.apply(person2))

// FUNCTION CURRYING
// creating a copy of a function but with some preset parameters
// function multiply (x, y) {
//   return x*y
// }

// var multiplyByTwo = multiply.bind(this, 2) // this will set param x in multiply to 2
// console.log(multiplyByTwo(3))


// FUNCTIONAL PROGRAMMING
// var arr1 = [1, 2, 3]
// console.log(arr1)

// var arr2 = []
// for (var i = 0; i < arr1.length; i++) {
//   arr2.push(arr1[i] * 2)
// }
// console.log(arr2)

// function mapForEach(arr, fn) {
//   var newArr = []
//   for (var j = 0; j < arr.length; j++) {
//     newArr.push(fn(arr[j]))
//   }
//   return newArr
// }

// var arr3 = mapForEach(arr1, function(el) {
//   return el * 3
// })
// console.log(arr3)

// var arr4 = mapForEach(arr1, function(el) {
//   return el % 2 === 0
// })
// console.log(arr4)

// function checkPastLimit (limiter, item) {
//   return item > limiter
// }

// // var limit = 5
// // var arr5 = mapForEach(arr2, function(el) {
// //   return checkPastLimit(limit, el)
// // })
// // console.log(arr5)

// var arr6 = mapForEach(arr2, checkPastLimit.bind(this, 4))
// console.log(arr6)

// function newCheckPast (limit) {
//   return function (limiter, item) {
//     return item > limiter
//   }.bind(this, limit) // so the returned function will always have the value being passed from outside when invoked
// }

// var arr7 = mapForEach(arr2, newCheckPast(3))
// console.log(arr7)

// TESTING UNDERSCORE LIBRARIES
var arr8 = _.map([1, 2, 3], function(el) { return el *4 })
console.log(arr8)

var arr9 = _.find(['a', 'b', 'c'], str => str === 'a')
console.log(arr9)
