// --------- NOTES: ---------
// JS HERE MAINLY REFERS TO FRONTEND JS WITH WINDOW AS GLOBAL OBJECT

// variables are attached to window object when it's not in a function on a base level

// when js is executed, 2 things happen, creation phase and execution phase
// creation phase ties all global vars to global object then value is assigned during execution phase
// as in, it creates a namespace to store the variable and functions (the whole thing)

// each fuction when invoked, creates an execution context. and this is added to the execution stack.
// the context on the top of the stack will get executed first before anything else

// block scope = everything between {}

// event queue will only be executed and cleared after execution stack is done
// the checking of event queue is called event loop

// async basically means that the code that runs outside of js engine
// ie: click events, http requests etc



function wait3Sec() {
  console.log('start waiting for 3 sec');
  var ms = 3000 + new Date().getTime();
  while (new Date() < ms) {}
  console.log('done waiting 3 sec');
}

function clickHandler() {
  console.log('clicked');
}

document.addEventListener('click', clickHandler);

wait3Sec();
console.log('finished all executions');
